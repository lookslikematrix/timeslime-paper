# timeslime

**timeslime** is a system to collect and manage your working time in all environments - in the office, home office or mobile.

[Glossary](./glossary.md)

## Requirements Overview

* Make it possible to measure the working hours of an person

* Develop a device to measure this on the desk without the need of a application

* Make the functionality available on a browser

* Make the functionality available on a command line interface

* If possible the solutions shouldn't have to be online

## Quality Goals

* Measure time accurate to at least seconds

* Synchronize all devices in 1 minute

## Stakeholders

| Role/Name             | Contact                             | Expectations                                                                               |
|-----------------------|-------------------------------------|--------------------------------------------------------------------------------------------|
| Employee              | christian.decker@lookslikematrix.de | Measure accurate working time and accessing reports about this data.                       |
| Supervisor            |                                     | Calculate salary for employee based on this data.                                          |
| Development & Support | christian.decker@lookslikematrix.de | Easy support and fast develop the system.                                                  |
| Competitor            |                                     | Mostly desktop applications and browser applications. Which are not quickly accessible.    |

# Architecture Constraints

* We want to use [Python](https://www.python.org/) for the CLI and the server backend

* We want to use [MicroPython](https://micropython.org/) for the embedded device, because we can then develop in the same language everywhere

* Python is easy to learn and hopefully we can develop a community based on this fact

* We want to use an ESP32 Board, because it's cheap and has an onboard WiFi

# System Scope and Context

In the following we describe the contexts of our system

## Business Context

~~~plantuml
@startuml
usecase timeslime

:Employee: --> timeslime :Measure time
:Employee: --> timeslime :Configure
:Development & Support: --> timeslime :develop\n&\nprovide support

timeslime --> :Supervisor: :Get reports\n&\nCalculate salary
timeslime --> :Employee: :Get reports
@enduml
~~~

## Building Block View

~~~plantuml
@startuml
actor "Development & Support" as development
actor "Employee" as employee_in
actor "Employee" as employee_out
actor "Supervisor" as supervisor
actor "Timeslime Server" as timeslime_server

component timeslime {
    portin "Configure" as configure_portin
    portin "Measure" as measure_portin

    component "Configure Service" as configure_service {
        portin "Configuration" as configuration_service_portin
        portout "Configuration" as configuration_service_portout
    }

    component "Time Service" as time_service {
        portin "Measure" as measure_time_service_portin
        portout "Measurement" as measurement_time_service_portout
    }

    component "Report Service" as report_service {
        portin "Configuration" as reports_configuration_portin
        portin "Measurement" as measurement_reports_portin
        portout "Reports" as reports_service_portout
    }

    component "Synchronize Service" as sync_service {
        portin "Configuration" as sync_configuration_portin
        portin "Measurement" as sync_measurement_portin

        portout "Configuration" as sync_configuration_portout
        portout "Measurement" as sync_measurement_portout
    }


    configure_portin --> configuration_service_portin
    configuration_service_portout --> reports_configuration_portin
    measurement_time_service_portout --> measurement_reports_portin
    measure_portin --> measure_time_service_portin

    configuration_service_portout <--> sync_configuration_portin
    measurement_time_service_portout <--> sync_measurement_portin

    portout "Report" as report_out
    portout "Server" as server_out

    reports_service_portout --> report_out
}

employee_in --> configure_portin
employee_in --> measure_portin
development ..> timeslime

report_out --> employee_out
report_out --> supervisor

sync_configuration_portout <--> server_out
sync_measurement_portout <--> server_out
server_out --> timeslime_server
@enduml
~~~

Inside the time service we need the possibilities to start and stop the time.

~~~plantuml
@startuml
component "Time Service" as time_service {
    portin "Measure" as measure_time_service_portin

    component Stop
    component Start

    Start -> Stop

    portout "Measurement" as measurement_time_service_portout

    measure_time_service_portin --> Stop
    measure_time_service_portin --> Start

    Start --> measurement_time_service_portout
    Stop --> measurement_time_service_portout
}
@enduml
~~~

Inside the synchronize service we need the possibilities to synchronize the measurements and the configurations. The state stores information about last synchronization time to reduce traffic.

~~~plantuml
@startuml
component "Synchronize Service" as sync_service {
    portin "Configuration" as sync_configuration_portin
    portin "Measurement" as sync_measurement_portin

    component "Synchronize" as synchronize
    component "State" as state

    portout "Configuration" as sync_configuration_portout
    portout "Measurement" as sync_measurement_portout

    sync_configuration_portin <--> synchronize
    sync_measurement_portin <--> synchronize

    state <-> synchronize : get/set last\nsynchronization time

    synchronize <--> sync_configuration_portout
    synchronize <--> sync_measurement_portout
}
@enduml
~~~

## Runtime View

The synchronization should be triggered automatically or manual. It should follow the following logic. Data is all we want to synchronize (e.g. measurements and configurations).

~~~plantuml
@startuml
actor User

User -> Local: trigger synchronization
Local -> Server: get all data which\nare newer than date X
Server -> Local: send all data which\nare newer then date X
Local -> Local: check if data_id exists\nif not add data
Local -> Local: check if data is newer on server\nif yes update data
Local -> Server: check if data is newer local\nand send data to server
Local --> User: response
@enduml
~~~